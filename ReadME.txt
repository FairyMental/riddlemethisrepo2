I hope everything is structured well enough to find your way around it.

Controls for game / additional information:

E - interact with objects (including opening doors, or checking what an item is)
P / Esc - Pause / Unpause game ( Also brings up options menu)
'/' - shoot fireworks (debugging purpose / testing )

Everything is should be intuitive enough.

***Easter Egg ***
To be found on the 4th house counting from the left, on the roof.(easily found in project scene)

PS: Lost some photoshop files on a flashdrive, so some raw assets are missing - except for the list of references,
nothing else has been imported /used from any other 3rd party source.

GIT Repo: https://bitbucket.org/FairyMental/riddlemethisrepo2 ( it is public )
Trello link: https://trello.com/b/EcXr6PRC/aint253-design-process
Game hosted online: http://fairymental.go.ro --> http://fairymental.bitbucket.org/riddlemethis.html

