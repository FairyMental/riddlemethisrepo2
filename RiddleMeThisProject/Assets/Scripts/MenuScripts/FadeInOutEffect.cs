﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeInOutEffect : MonoBehaviour {

    private float ratio = 1f / 60f;

    private Image img;
    private bool state = true;
    private float curAlpha = 1f;
	// Use this for initialization
	void Start () {
        img = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (img.enabled)
        {
            if (state)
            {
                curAlpha -= ratio;
                img.color = new Color(1, 1, 1, curAlpha);
            }
            else
            {
                curAlpha += ratio;
                img.color = new Color(1, 1, 1, curAlpha);
            }
            if (curAlpha <= 0)
                state = false;
            else if (curAlpha >= 1)
                state = true;
        }
	}
}
