﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class EndMenuButtons : MonoBehaviour {

    // Use this for initialization
    private AudioSource aSource;

	void Start () {
        aSource = GetComponent<AudioSource>();
	}
	
	public void OnPlayAgainClick()
    {
        aSource.Play();
        SceneManager.LoadScene(1);
    }
    public void OnReturnClick()
    {
        aSource.Play();
        SceneManager.LoadScene(0);
    }
}
