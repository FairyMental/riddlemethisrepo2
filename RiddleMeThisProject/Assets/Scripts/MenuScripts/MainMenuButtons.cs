﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
namespace UnityStandardAssets.Characters.FirstPerson
{
    public class MainMenuButtons : MonoBehaviour
    {

        private bool optionsON = false;
        public GameObject options;
        public AudioClip MenuSelectEffect;
        public Canvas pauseCanvas;
        private AudioSource aSource;


        void Start()
        {
            aSource = GetComponent<AudioSource>();
        }

        public void OnPlayPress()
        {
            SceneManager.LoadScene(1);

            PlayMenuSelectEffect();
        }
        public void OnOptionsPress()
        {
            optionsON = !optionsON;
            pauseCanvas.enabled = !pauseCanvas.enabled;
            GameObject.Find("PauseUI/Pause").GetComponent<Pausescript>().gamePause = optionsON;
            PlayMenuSelectEffect();
        }

        public void OnQuitPress()
        {
            PlayMenuSelectEffect();
            Application.Quit();
        }

        public void PlayMenuSelectEffect()
        {
            aSource.PlayOneShot(MenuSelectEffect, 1.0f);

        }
    }
}