﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;
namespace UnityStandardAssets.Characters.FirstPerson
{
    public class Pausescript : MonoBehaviour {

        public Light brightnessLight;
        public bool calculateBrightness = false;

        public AudioMixer masterMixer;
        public GameObject[] GOSliders;

        public Canvas pauseCanvas;
        public Canvas UICanvas;

        private Slider[] sliders;
        public bool gamePause = false;

        // Use this for initialization
        void Start() {
            sliders = new Slider[GOSliders.Length];
            for (int i = 0; i < sliders.Length; i++)
            {
                sliders[i] = GOSliders[i].GetComponent<Slider>();
            }
        }

        // Update is called once per frame
        void Update() {
            if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
            {
                gamePause = !gamePause;
            }
            if (gamePause)
            {
                masterMixer.SetFloat("VolumeMaster", sliders[0].value * 100 - 80);
                masterMixer.SetFloat("VolumeMusic", sliders[1].value * 100 - 80);
                masterMixer.SetFloat("VolumeVoice", sliders[2].value * 100 - 80);
                masterMixer.SetFloat("VolumeOutside", sliders[3].value * 100 - 80);
                masterMixer.SetFloat("VolumeInside", sliders[3].value * 100 - 100);
                if(calculateBrightness)
                    brightnessLight.intensity = sliders[4].value;
                Cursor.visible = true;


            }

            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)) && gamePause == true && calculateBrightness == true)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>().m_MouseLook.lockCursor = true;
                Cursor.visible = false;
            }
        }
    }
}
