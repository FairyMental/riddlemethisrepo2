﻿using UnityEngine;
using System.Collections;
namespace UnityStandardAssets.Characters.FirstPerson
{
    
    public class OnCollisionEnter : MonoBehaviour
    {

        GameSystem gs;

        void Start()
        {
            gs = GameObject.FindGameObjectWithTag("GameSystem").GetComponent<GameSystem>();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player" )
            {
                if (gameObject.tag == "InsideTrigger")
                    gs.setInside();
                else if (gameObject.tag == "OutsideTrigger")
                    gs.setOutside();
                    
            }
            
        }

    }
}
