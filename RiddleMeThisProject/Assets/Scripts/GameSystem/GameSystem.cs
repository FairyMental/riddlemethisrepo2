﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public class GameSystem : MonoBehaviour
    {
        private FirstPersonController fpsController;
        private AudioSource aSource;
        private Transform[] RiddleZoneTf;
        private GameObject[] RiddleZones;
        private int[] riddlesPlayed;

        private int crtRiddle = -1;
        private int attemptsLeft = 0;
        private int riddlesSolved = 0;
        private int riddlesLeft = 0;

        private bool replayingRiddle = false;
        private bool riddleStarted = false;
        public bool isTyping = false;
        private bool firstLetterInput = true;

        private string auxString = "";
        private string answer = "";

        private bool isInside = false;
        private GameObject[] weatherObjects;

        public AudioMixerGroup insideMixer;
        public AudioMixerGroup outsideMixer;

        public AudioClip[] insideFootsteps;
        public AudioClip[] outsideFootsteps;
        public AudioClip insideLand;
        public AudioClip outsideLand;


        public Transform playerTf;

        public Text MessageBox;
        public Text typingText;

        public AudioClip[] Riddles;
        public string[] RiddleAnswers;
        private int totalRiddles;

        public AudioClip IntroVoiceFemale;
        public AudioClip RiddleFailed;
        public AudioClip RiddleSolved;
        public AudioClip OneAttemptLeft;
        public AudioClip SolvedAllRiddles;
        public AudioClip SolvedLessRiddles;
        public AudioClip SolvedNoRiddles;
        public AudioClip WrongAnswer;
        public AudioClip IntroScenario;

        public Canvas UICanvas;
        public Canvas PauseCanvas;
        public bool gamePaused = false;

        private Ray ray;
        private RaycastHit hit;

        public Image[] dotsUI;
        public Image[] crossesUI;
        public Image[] ticksUI;

        public Image[] boxChecks;
        public Color failedAttemptColor = new Color(120f / 255f, 0, 0, 1);
        public Color availableAttemptColor = new Color(0, 120f / 255f, 0, 1);

        private int uiCount = 0;
        // Use this for initialization

        void RefreshAttemptUI()
        {
            for (int i = 0; i < 3; i++)
            {
                boxChecks[i].color = failedAttemptColor;
            }

            for (int i = 0; i < attemptsLeft; i++)
            {
                boxChecks[i].color = availableAttemptColor;
            }
        }

        void Start()
        {

            aSource = GetComponent<AudioSource>();
            RiddleZones = GameObject.FindGameObjectsWithTag("RiddleZone");
            riddlesLeft = RiddleZones.Length;
            totalRiddles = riddlesLeft;
            RiddleZoneTf = new Transform[RiddleZones.Length];

            for (int i = 0; i < dotsUI.Length; i++)
            {
                dotsUI[i].enabled = false;
                crossesUI[i].enabled = false;
                ticksUI[i].enabled = false;
            }
            RefreshAttemptUI();


            fpsController = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();

            weatherObjects = GameObject.FindGameObjectsWithTag("Weather");

            for (int i = 0; i < RiddleZones.Length; i++)
            {
                RiddleZoneTf[i] = RiddleZones[i].GetComponent<Transform>();
            }

            riddlesPlayed = new int[Riddles.Length];
            for (int i = 0; i < riddlesPlayed.Length; i++)
            {
                riddlesPlayed[i] = 0;
            }
            Debug.Log("Initialized Riddle: " + Riddles.Length + "  " + RiddleZoneTf.Length + "   " + riddlesPlayed.Length);

            Invoke("PlayIntroScenario", 1.0f);
            InvokeRepeating("CheckRiddleZones", 7.0f, 0.5f);

            
        }

        void PlayIntroScenario()
        {
            aSource.PlayOneShot(IntroScenario, 1.0f);
            Invoke("PlayIntroScenario2", 18.0f);
        }

        void PlayIntroScenario2()
        {
            aSource.PlayOneShot(IntroVoiceFemale, 1.0f);
            Invoke("GiveAdvice", 4.0f);
        }

        void GiveAdvice()
        {
            sendMsg("Check houses for green particles, enter these zones to receive a riddle.", 4.0f);
        }

        void GoToEndScene()
        {
            SceneManager.LoadScene(2);
        }

        void sendMsg(string message, float displayDuration)
        {
            Color auxColor = MessageBox.color;
            auxColor.a = 1.0f;
            MessageBox.color = auxColor;

            MessageBox.text = message;
            InvokeRepeating("FadeMessage", displayDuration, 0.1f);
        }

        void FadeMessage()
        {
            Color auxColor = MessageBox.color;
            auxColor.a -= 0.1f;
            MessageBox.color = auxColor;

            if (auxColor.a < 0)
                CancelInvoke("FadeMessage");
        }

        void Update()
        {

            ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
            if (Physics.Raycast(ray, out hit) && Input.GetKeyDown(KeyCode.E) && isTyping == false)
            {
                sendMsg("That is a " + hit.collider.gameObject.tag.ToString(),4f);
            }

            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)) && isTyping == false)
            {
                fpsController.m_MouseLook.lockCursor = !fpsController.m_MouseLook.lockCursor;
                UICanvas.enabled = !UICanvas.enabled;
                PauseCanvas.enabled = !UICanvas.enabled;
                
               
            }

                if (Input.GetKeyDown(KeyCode.R) && crtRiddle != -1 && replayingRiddle == false && isTyping == false)
                {
                    aSource.PlayOneShot(Riddles[crtRiddle], 1.0f);
                    replayingRiddle = true;
                    Invoke("UpdateReplay", 20.0f);
                }


                if (isTyping && Input.anyKeyDown)
                {
                    if (firstLetterInput == false)
                    {
                        typingText.text = "";
                        firstLetterInput = true;
                    }

                    if (Input.GetKeyDown(KeyCode.Backspace))
                    {
                        typingText.text = typingText.text.Substring(0, typingText.text.Length - 1);
                    }
                    else if (Input.GetKeyDown(KeyCode.Return))
                    {
                        answer = typingText.text;
                        if (AnswerMatching(answer, RiddleAnswers[crtRiddle]))
                        {
                            riddlesLeft--;
                            
                            riddlesSolved++;

                            crtRiddle = -1;
                            riddleStarted = false;
                            if (riddlesLeft == 0)
                            {
                                if (riddlesSolved == totalRiddles)
                                    aSource.PlayOneShot(SolvedAllRiddles, 1.0f);
                                else if (riddlesSolved >= totalRiddles / 2)
                                    aSource.PlayOneShot(SolvedLessRiddles, 1.0f);
                                else
                                    aSource.PlayOneShot(SolvedNoRiddles, 1.0f);
                                Invoke("GoToEndScene", 8.0f);
                                GameObject.FindGameObjectWithTag("FireworkSystem").GetComponent<Fireworks>().ShootFireworks();
                                Cursor.visible = true;
                            }
                            else
                            {
                                aSource.PlayOneShot(RiddleSolved);
                                dotsUI[uiCount].enabled = false;
                                ticksUI[uiCount++].enabled = true; 
                            }
                            RefreshAttemptUI();

                        }
                        else
                        {
                            attemptsLeft--;

                            if (attemptsLeft == 0)
                            {
                                crtRiddle = -1;
                                riddleStarted = false;
                                riddlesLeft--;

                                if (riddlesLeft == 0)
                                {
                                    SceneManager.LoadScene(2);
                                }
                                aSource.PlayOneShot(RiddleFailed, 1.0f);

                                dotsUI[uiCount].enabled = false;
                                crossesUI[uiCount++].enabled = true;

                            }
                            else if (attemptsLeft == 1)
                            {
                                aSource.PlayOneShot(OneAttemptLeft, 1.0f);
                            }
                            else
                            {
                                aSource.PlayOneShot(WrongAnswer, 1.0f);
                            }
                            RefreshAttemptUI();
                        }


                        isTyping = false;
                        GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>().enabled = true;

                        typingText.text = "Press Enter to type answer";
                        typingText.color = new Color(120f, 120f, 120f);

                    }
                    else
                    {
                        typingText.text = typingText.text + Input.inputString;
                    }

                }
                else if (riddleStarted && Input.GetKeyDown(KeyCode.Return) && isTyping == false)
                {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>().enabled = false;
                    isTyping = true;
                    typingText.color = new Color(255.0f, 255.0f, 255.0f);
                    typingText.text = "|";
                    firstLetterInput = false;
                }
                else
                {
                    //Debug.Log(riddleStarted + " " + isTyping);
                }
            
        }

        private bool AnswerMatching(string playerAnswer, string riddleAnswer)
        {
            bool test1 = false;
            bool test2 = false;
            int correctPosChar = 0;

            string[] riddleAnswers = riddleAnswer.Split('-');
            for (int i = 0; i < riddleAnswers.Length; i++)
            {
                test1 = false;
                test2 = false;

                while (riddleAnswers[i].Length < playerAnswer.Length)
                {
                    riddleAnswers[i] = riddleAnswers[i] + " ";
                }
                while (riddleAnswers[i].Length > playerAnswer.Length)
                {
                    playerAnswer = playerAnswer + " ";
                }

                if (playerAnswer.Length == riddleAnswers[i].Length || playerAnswer.Length == riddleAnswers[i].Length - 1 || playerAnswer.Length == riddleAnswers[i].Length + 1)
                {
                    test1 = true;
                }
                for (int j = 0; j < riddleAnswers[i].Length; j++)
                {
                    if (j == 0)
                    {
                        if (playerAnswer[j] == riddleAnswers[i][j] || playerAnswer[j] == riddleAnswers[i][j + 1])
                        {
                            correctPosChar++;
                        }
                    }
                    else if (j == riddleAnswers[i].Length - 1)
                    {
                        if (playerAnswer[j] == riddleAnswers[i][j] || playerAnswer[j] == riddleAnswers[i][j - 1])
                        {
                            correctPosChar++;
                        }
                    }
                    else
                    {
                        if (playerAnswer[j] == riddleAnswers[i][j] || playerAnswer[j] == riddleAnswers[i][j - 1] || playerAnswer[j] == riddleAnswers[i][j + 1])
                        {
                            correctPosChar++;
                        }
                    }
                }
                if (correctPosChar + 3 > riddleAnswers[i].Length)
                {
                    test2 = true;
                }
                if (test1 && test2)
                {
                    return true;
                }
            }

            return false;
        }

        void UpdateReplay()
        {
            replayingRiddle = false;
            sendMsg("Press \'R\' to replay the riddle.", 4.0f);
        }

        public void CheckRiddleZones()
        {
            if (riddleStarted == false)
            {
                for (int i = 0; i < RiddleZoneTf.Length; i++)
                {
                    if (Vector3.Distance(playerTf.position, RiddleZoneTf[i].position) < 3)
                    {

                        RiddleZoneTf[i].position = new Vector3(-1000, -1000, -1000);

                        crtRiddle = pickRiddle();
                        aSource.PlayOneShot(Riddles[crtRiddle], 1.0f);

                        print(uiCount);
                        dotsUI[uiCount].enabled = true;

                        riddleStarted = true;
                        attemptsLeft = 3;
                        replayingRiddle = true;
                        RefreshAttemptUI();
                        Invoke("UpdateReplay", 20.0f);

                    }
                }
            }
            else
            {


            }
        }

        public int pickRiddle()
        {
            int index = -1;

            do
            {
                index = Random.Range(0, Riddles.Length);
            } while (riddlesPlayed[index] != 0);
            riddlesPlayed[index] = 1;

            return index;
        }

        public void setInside()
        {
            isInside = true;
            for (int i = 0; i < weatherObjects.Length; i++)
            {
                weatherObjects[i].GetComponent<AudioSource>().outputAudioMixerGroup = insideMixer;
            }
            GameObject.FindGameObjectWithTag("ReverbZone").GetComponent<AudioReverbZone>().reverbPreset = AudioReverbPreset.CarpetedHallway;
            fpsController.m_FootstepSounds[0] = insideFootsteps[0];
            fpsController.m_FootstepSounds[1] = insideFootsteps[1];

            fpsController.m_LandSound = insideLand;
            
        }
        public void setOutside()
        {
            isInside = false;
            for (int i = 0; i < weatherObjects.Length; i++)
            {
                weatherObjects[i].GetComponent<AudioSource>().outputAudioMixerGroup = outsideMixer;
            }
            GameObject.FindGameObjectWithTag("ReverbZone").GetComponent<AudioReverbZone>().reverbPreset = AudioReverbPreset.Off;

            fpsController.m_FootstepSounds[0] = outsideFootsteps[0];
            fpsController.m_FootstepSounds[1] = outsideFootsteps[1];

            fpsController.m_LandSound = outsideLand;
        }
    }
}