﻿using UnityEngine;
using System.Collections;

public class RotateCamera : MonoBehaviour {

    private Transform camTf;
    private float rot = 0f;

    public float rotSpeed = 0.3f;

	void Start () {
        camTf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        rot += rotSpeed;
        camTf.rotation = Quaternion.Euler(-90f, rot, 0f);
        if (rot > 360)
            rot = 0;
	}
}
