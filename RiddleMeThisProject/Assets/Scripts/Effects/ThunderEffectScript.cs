﻿using UnityEngine;
using System.Collections;

public class ThunderEffectScript : MonoBehaviour {

    public Material normalSkybox;
    public Material skyboxWithLightning;
    public Light Dlight;

    public float maxLightIntensity = 2.0f;
    public float minLightIntensity = 0.15f;  


    private AudioSource aSource;
    private bool doEffect = false;
    private int frameCount = 0;
    private int relapseSkyBox = 0;
    void Start()
    {
        aSource = GetComponent<AudioSource>();
        Invoke("PlaySoundEffect", 3.0f);
    }

    void Update()
    {
        if(doEffect == true)
        {
            
            frameCount++;
            if (frameCount > 3)
            {
                if (relapseSkyBox % 2 == 0)
                {
                    RenderSettings.skybox = skyboxWithLightning;
                    Dlight.intensity = maxLightIntensity;
                }
                else
                {
                    RenderSettings.skybox = normalSkybox;
                    Dlight.intensity = minLightIntensity;
                }

                relapseSkyBox++;
                frameCount = 0;

                if (relapseSkyBox == 6)
                {
                    doEffect = false;
                    relapseSkyBox = 0;
                    frameCount = 0;
                    Invoke("PlaySoundEffect", Random.Range(20, 30));
                }
            }
            
        }

    }

    private void updateDoEffect()
    {   
        doEffect = true;
    }
    private void PlaySoundEffect()
    {
        aSource.Play();
        Invoke("updateDoEffect", 0.5f);
    }
}
