﻿using UnityEngine;
using System.Collections;

public class EasterEggScript : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
