﻿using UnityEngine;
using System.Collections;

public class Fireworks : MonoBehaviour {

    public float duration = 10f;

    public GameObject[] fireworks;


	// Use this for initialization
	void Start () {

        fireworks = GameObject.FindGameObjectsWithTag("Firework");
        for (int i = 0; i < fireworks.Length; i++)
        {
            fireworks[i].SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Slash))
        {
            ShootFireworks();
        }
	}
    public void ShootFireworks()
    {
        for (int i = 0; i < fireworks.Length; i++)
        {
            fireworks[i].SetActive(true);
        }
        Invoke("StopFireworks", duration);
    }
    public void StopFireworks()
    {
        for (int i = 0; i < fireworks.Length; i++)
        {
            fireworks[i].SetActive(false);
        }
    }
}
