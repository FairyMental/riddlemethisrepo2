﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RandomTextColors : MonoBehaviour {

    public float speedRate = 1.0f;

    private string originalText;
    private string coloredText;
    private char[] chars;
    private string[] coloredChar;


    private Color[] colors;
    private int[] locks;

    private float random;

    private string auxString;
	// Use this for initialization
	void Start () {
        originalText = GetComponent<Text>().text;
        chars = GetComponent<Text>().text.ToCharArray();
        coloredChar = new string[chars.Length];
        colors = new Color[chars.Length];
        locks = new int[chars.Length];

        for (int i = 0; i < chars.Length; i++)
        {
            if (i % 6 == 0)
            {
                colors[i] = new Color(1.0f, 0.0f, 0.0f);
                locks[i] = 1;
            }
            if (i % 6 == 1)
            {
                colors[i] = new Color(0.0f, 1.0f, 0.0f);
                locks[i] = 2;
            }
            if (i % 6 == 2)
            {
                colors[i] = new Color(0.0f, 0.0f, 1.0f);
                locks[i] = 3;
            }
            if (i % 6 == 3)
            {
                colors[i] = new Color(1.0f, 1.0f, 0.0f);
                locks[i] = 4;
            }
            if (i % 6 == 4)
            {
                colors[i] = new Color(1.0f, 0.0f, 1.0f);
                locks[i] = 5;
            }
            if (i % 6 == 5)
            {
                colors[i] = new Color(0.0f, 1.0f, 1.0f);
                locks[i] = 6;
            }
        }
	}
    public static string colorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }
 

	// Update is called once per frame
	void Update ()
    {

        for (int i = 0; i < chars.Length; i++)
        {
            // r = 1 , g-> 1
            if (locks[i] == 1)
            {
                colors[i].g += 1.0f / (255.0f / speedRate) ;
                if (colors[i].g >= 1.0f)
                {
                    colors[i].g = 1.0f;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 4;
                    else
                        locks[i] = 10;
                }
            }
            // g = 1, b->1
            if (locks[i] == 2)
            {
                colors[i].b += 1.0f / (255.0f / speedRate);
                if (colors[i].b >= 1.0f)
                {
                    colors[i].b = 1.0f;
                    locks[i] = 0;

                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 6;
                    else
                        locks[i] = 12;
                }
            }
            // b = 1, r->1
            if (locks[i] == 3)
            {
                colors[i].r += 1.0f / (255.0f / speedRate);
                if (colors[i].r >= 1.0f)
                {
                    colors[i].r = 1.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 5;
                    else
                        locks[i] = 11;
                }
            }
            // r = 1, b--> 1
            if (locks[i] == 7)
            {
                colors[i].b += 1.0f / (255.0f / speedRate);
                if (colors[i].b >= 1.0f)
                {
                    colors[i].b = 1.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 5;
                    else
                        locks[i] = 11;
                }
            }
            // g = 1, r -> 1
            if (locks[i] == 8)
            {
                colors[i].r += 1.0f / (255.0f / speedRate);
                if (colors[i].r >= 1.0f)
                {
                    colors[i].r = 1.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 4;
                    else
                        locks[i] = 10;
                }
            }
            // b = 1, g->1
            if (locks[i] == 9)
            {
                colors[i].g += 1.0f / (255.0f / speedRate);
                if (colors[i].g >= 1.0f)
                {
                    colors[i].g = 1.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 6;
                    else
                        locks[i] = 12;
                }
            }





            // r=1, g=1, g->0 
            if (locks[i] == 4)
            {
                colors[i].g -= 1.0f / (255.0f / speedRate);
                if (colors[i].g <= 0.0f)
                {
                    colors[i].g = 0.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 1;
                    else
                        locks[i] = 7;
                }
            }
            // r=1,b=1, b->0
            if (locks[i] == 5)
            {
                colors[i].b -= 1.0f / (255.0f / speedRate);
                if (colors[i].b <= 0.0f)
                {
                    colors[i].b = 0.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 1;
                    else
                        locks[i] = 7;
                }
            }
            // g=1,b=1, b-->0
            if (locks[i] == 6)
            {
                colors[i].b -= 1.0f / (255.0f / speedRate);
                if (colors[i].b <= 0.0f)
                {
                    colors[i].b = 0.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 2;
                    else
                        locks[i] = 8;
                }
            }
            // r=1,g=1, r->0
            if (locks[i] == 10)
            {
                colors[i].r -= 1.0f / (255.0f / speedRate);
                if (colors[i].r <= 0.0f)
                {
                    colors[i].r = 0.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 2;
                    else
                        locks[i] = 8;
                }
            }
            // r=1,b=1, r->0
            if (locks[i] == 11)
            {
                colors[i].r -= 1.0f / (255.0f / speedRate);
                if (colors[i].r <= 0.0f)
                {
                    colors[i].r = 0.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 3;
                    else
                        locks[i] = 9;
                }
            }
            // g=1,b=1, g->0
            if (locks[i] == 12)
            {
                colors[i].g -= 1.0f / (255.0f / speedRate);
                if (colors[i].g <= 0.0f)
                {
                    colors[i].g = 0.0f;
                    locks[i] = 0;
                    random = Random.Range(0, 99);
                    if (random >= 50)
                        locks[i] = 3;
                    else
                        locks[i] = 9;
                }
            }
        }



        coloredText = "";
        for (int i = 0; i < chars.Length; i++)
        {
            coloredChar[i] = "<color=#" + colorToHex(colors[i]) + ">" + chars[i] + "</color>";
            coloredText += coloredChar[i];
        }
        GetComponent<Text>().text = coloredText;

	}
    public void OnHoverEnter()
    {
        GetComponent<RandomTextColors>().enabled = true;
    }
    public void OnHoverExit()
    {
        GetComponent<RandomTextColors>().enabled = false;
        GetComponent<Text>().text = originalText;
    }
}

