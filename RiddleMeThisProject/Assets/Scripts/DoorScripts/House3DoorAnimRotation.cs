﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class House3DoorAnimRotation : MonoBehaviour {

    public AudioClip closingEffect;
    public Text MessageBox;

    public Transform playerTf;
    public float angleToRotate = 90.0f;
    public float animSpeed = 1.0f;
    public float animTime = 1.0f;
    public int frames = 30;

    private BoxCollider boxCollider;

    private AudioSource aSource;
    private float totalRotation = 0.0f;
    private Transform tf;
    private int crtFrame = 0;
    private bool isAnimating = false;
    private bool isClosed = true;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
        aSource = GetComponent<AudioSource>();
        boxCollider = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Vector3.Distance(playerTf.position, tf.position) < 4)
        {
            sendMsg("Press \'E\' to open the door.", 4.0f);
            if (Input.GetKeyDown(KeyCode.E))
            {

                if (isClosed && isAnimating == false)
                {
                    boxCollider.enabled = false;
                    InvokeRepeating("OpenDoor", 0.0f, animTime / frames);
                    isAnimating = true;
                    aSource.Play();
                }
                else if (!isClosed && isAnimating == false)
                {
                    boxCollider.enabled = true;
                    InvokeRepeating("CloseDoor", 0.0f, animTime / frames);
                    isAnimating = true;
                    aSource.PlayOneShot(closingEffect, 1.0f);
                }
            }
        }
	}
    void OpenDoor()
    {
        totalRotation += angleToRotate / frames;
        Quaternion newRot = Quaternion.Euler(0, 0, totalRotation);
        tf.localRotation = newRot;

        crtFrame++;
        if(frames == crtFrame)
        {
            isAnimating = false;
            isClosed = false;
            crtFrame = 0;
            CancelInvoke("OpenDoor");
        }

    }
    void CloseDoor()
    {
        totalRotation -= angleToRotate / frames;
        Quaternion newRot = Quaternion.Euler(0, 0, totalRotation);
        tf.localRotation = newRot;

        crtFrame++;
        if (frames == crtFrame)
        {
            isAnimating = false;
            isClosed = true;
            crtFrame = 0;
            CancelInvoke("CloseDoor");
        }
    }
    void sendMsg(string message, float displayDuration)
    {
        Color auxColor = MessageBox.color;
        auxColor.a = 1.0f;
        MessageBox.color = auxColor;

        MessageBox.text = message;
        InvokeRepeating("FadeMessage", displayDuration, 0.1f);
    }
    void FadeMessage()
    {
        Color auxColor = MessageBox.color;
        auxColor.a -= 0.1f;
        MessageBox.color = auxColor;

        if (auxColor.a < 0)
            CancelInvoke("FadeMessage");
    }

}

