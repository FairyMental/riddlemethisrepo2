﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class House2SlidingDoorAnim : MonoBehaviour {

    public Transform playerTf;
    public float animSpeed = 1;
    public bool xAxis;
    public bool moveTowardsNegative;
    public Text MessageBox;

    private Transform tf;
    private bool isClosed = true;
    private float animRatio = 0.035f;
    private bool isAnimating = false;
    private int crtIteration = 0;
    private int maxIterations = 17;

	// Update is called once per frame
    void Start()
    {
        tf = GetComponent<Transform>();
    }

	void Update () {
        if (Vector3.Distance(playerTf.position, tf.position) < 2)
        {
            sendMsg("Press \'E\' to open the door.", 4.0f);
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("E pressed");
                if (isClosed && isAnimating == false)
                {
                    InvokeRepeating("OpenDoor", 0f, 0.05f * animSpeed);
                    GetComponent<AudioSource>().Play();
                    isAnimating = true;
                }
                else if (!isClosed && isAnimating == false)
                {
                    InvokeRepeating("CloseDoor", 0f, 0.05f * animSpeed);
                    isAnimating = true;
                    GetComponent<AudioSource>().Play();
                }
            }
        }
	}
    void OpenDoor()
    { 
        if(moveTowardsNegative == false)
        {
            animRatio = -0.035f;
        }
        if(xAxis == true)
        {
            Vector3 newPos = new Vector3(tf.localPosition.x + animRatio, tf.localPosition.y, tf.localPosition.z);
            tf.localPosition = newPos;
        }
        else
        {
            Vector3 newPos = new Vector3(tf.localPosition.x , tf.localPosition.y + animRatio, tf.localPosition.z );
            tf.localPosition = newPos;
        }
        crtIteration++;

        if(crtIteration == maxIterations)
        {
            CancelInvoke("OpenDoor");
            crtIteration = 0;
            isClosed = false;
            isAnimating = false;
        }
    }
    void CloseDoor()
    {
        if(moveTowardsNegative == false)
        {
            animRatio = -0.035f;
        }
        if (xAxis == true)
        {
            Vector3 newPos = new Vector3(tf.localPosition.x - animRatio, tf.localPosition.y, tf.localPosition.z);
            tf.localPosition = newPos;
        }
        else
        {
            Vector3 newPos = new Vector3(tf.localPosition.x, tf.localPosition.y - animRatio, tf.localPosition.z );
            tf.localPosition = newPos;
        }
        crtIteration++;

        if (crtIteration == maxIterations)
        {
            CancelInvoke("CloseDoor");
            crtIteration = 0;
            isClosed = true;
            isAnimating = false;
        }
    }
    void sendMsg(string message, float displayDuration)
    {
        Color auxColor = MessageBox.color;
        auxColor.a = 1.0f;
        MessageBox.color = auxColor;

        MessageBox.text = message;
        InvokeRepeating("FadeMessage", displayDuration, 0.1f);
    }
    void FadeMessage()
    {
        Color auxColor = MessageBox.color;
        auxColor.a -= 0.1f;
        MessageBox.color = auxColor;

        if (auxColor.a < 0)
            CancelInvoke("FadeMessage");
    }
}
