﻿using UnityEngine;
using System.Collections;

public class House2DoorAnim : MonoBehaviour {

    public Transform playerTf;
    public float animSpeed = 1;
    public bool xAxis;
    public bool moveTowardsNegative;

    private Transform tf;
    private bool isClosed = true;
    private float animRatio = 0.035f;
    private bool isAnimating = false;
    private int crtIteration = 0;
    private int maxIterations = 17;

	// Update is called once per frame
    void Start()
    {
        tf = GetComponent<Transform>();
    }

	void Update () {
        if (Input.GetKeyDown(KeyCode.E) && Vector3.Distance(playerTf.position,tf.position) < 2)
        {
            Debug.Log("E pressed");
            if (isClosed && isAnimating == false)
            {
                InvokeRepeating("OpenDoor", 0f, 0.05f * animSpeed);
                isAnimating = true;
            }
            else if (!isClosed && isAnimating == false)
            {
                InvokeRepeating("CloseDoor", 0f, 0.05f * animSpeed);
                isAnimating = true;
            }
        }
	}
    void OpenDoor()
    { 
        if(moveTowardsNegative == false)
        {
            animRatio = -0.035f;
        }
        if(xAxis == true)
        {
            Vector3 newPos = new Vector3(tf.localPosition.x + animRatio, tf.localPosition.y, tf.localPosition.z);
            tf.localPosition = newPos;
        }
        else
        {
            Vector3 newPos = new Vector3(tf.localPosition.x , tf.localPosition.y + animRatio, tf.localPosition.z );
            tf.localPosition = newPos;
        }
        crtIteration++;

        if(crtIteration == maxIterations)
        {
            CancelInvoke("OpenDoor");
            crtIteration = 0;
            isClosed = false;
            isAnimating = false;
        }
        Debug.Log("Iterated");
    }
    void CloseDoor()
    {
        if(moveTowardsNegative == false)
        {
            animRatio = -0.035f;
        }
        if (xAxis == true)
        {
            Vector3 newPos = new Vector3(tf.localPosition.x - animRatio, tf.localPosition.y, tf.localPosition.z);
            tf.localPosition = newPos;
        }
        else
        {
            Vector3 newPos = new Vector3(tf.localPosition.x, tf.localPosition.y - animRatio, tf.localPosition.z );
            tf.localPosition = newPos;
        }
        crtIteration++;

        if (crtIteration == maxIterations)
        {
            CancelInvoke("CloseDoor");
            crtIteration = 0;
            isClosed = true;
            isAnimating = false;
        }
    }
}
